package polinoame;

 

import java.util.ArrayList;

public class Polinom {
	ArrayList<Monom> m=new ArrayList<Monom>();
	
	public Polinom(ArrayList<Monom> m){
		this.m=m;
	}
	
	public ArrayList<Monom> getList(){
		return m;
	}
	
	
	
	public void adaugaMonom(Monom x){
		m.add(x);
	}
	
	public String toString(){
		String rez=new String();
		for(int i=0; i<m.size();i++){
			if(m.get(i).getCoef()>0)
			{
			rez+="+";
			}
			rez+=m.get(i).toString();
			rez+=" ";
		}
		return rez;
	}

}
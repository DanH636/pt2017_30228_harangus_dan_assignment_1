package polinoame;

 

public class Monom implements Comparable<Monom>{
	float coef;
	int exp;
	
	public Monom(float coef, int exp){
		this.coef=coef;
		this.exp=exp;
	}
	public float getCoef() {
		return this.coef;
	}
	public void setCoef(float coef) {
		this.coef=coef;
	}
	public int getExp() {
		return this.exp;
	}
	public void setExp(int exp) {
		this.exp=exp;
	}
	

	
	public String toString(){
	
		return String.format("%fx^%d",this.coef, this.exp);
		
	}
	@Override
	public int compareTo(Monom o) {
		// TODO Auto-generated method stub
		return Integer.compare(o.getExp(),this.getExp());
		
	}	

}
package polinoame;




import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import org.junit.Test;

public class TestJunit {
    @Test
    public void adunare() {
        ArrayList<Monom> lista = new ArrayList<>();
        ArrayList<Monom> lista2 = new ArrayList<>();
        Monom m1 = new Monom(2, 2);
        Monom m2 = new Monom(3, 4);
        Monom m3 = new Monom(3, 5);
        Monom m4 = new Monom(4, 4);

        lista.add(m1);
        lista.add(m2);
        lista2.add(m3);
        lista2.add(m4);
        Polinom p1 = new Polinom(lista);
        Polinom p2 = new Polinom(lista2);

        assertEquals("+3,000000x^5 +7,000000x^4 +2,000000x^2 ", OperatiiPolinoame.add(p1, p2).toString());
    }
    @Test
    public void scadere() {
        ArrayList<Monom> lista = new ArrayList<>();
        ArrayList<Monom> lista2 = new ArrayList<>();
        Monom m1 = new Monom(2, 2);
        Monom m2 = new Monom(3, 4);
        Monom m3 = new Monom(3, 5);
        Monom m4 = new Monom(4, 4);

        lista.add(m1);
        lista.add(m2);
        lista2.add(m3);
        lista2.add(m4);
        Polinom p1 = new Polinom(lista);
        Polinom p2 = new Polinom(lista2);

        assertEquals("-0,600000x^5 -6,000000x^4 +0,666667x^3 -4,000000x^2 ",OperatiiPolinoame.sub(p1,p2).toString());
    }
    @Test
    public void inm() {
        ArrayList<Monom> lista = new ArrayList<>();
        ArrayList<Monom> lista2 = new ArrayList<>();
        Monom m1 = new Monom(2, 2);
        Monom m2 = new Monom(3, 4);
        Monom m3 = new Monom(3, 5);
        Monom m4 = new Monom(4, 4);

        lista.add(m1);
        lista.add(m2);
        lista2.add(m3);
        lista2.add(m4);
        Polinom p1 = new Polinom(lista);
        Polinom p2 = new Polinom(lista2);

        assertEquals("+9,000000x^10 +12,000000x^9 +21,000000x^9 +28,000000x^8 +6,000000x^7 +8,000000x^6 +6,000000x^7 +8,000000x^6 +9,000000x^9 +12,000000x^8 ",OperatiiPolinoame.inm(p1,p2).toString());
    }
    @Test
    public void integrare() {
        ArrayList<Monom> lista = new ArrayList<>();
        ArrayList<Monom> lista2 = new ArrayList<>();
        Monom m1 = new Monom(2, 2);
        Monom m2 = new Monom(3, 4);
        Monom m3 = new Monom(3, 5);
        Monom m4 = new Monom(4, 4);

        lista.add(m1);
        lista.add(m2);
        lista2.add(m3);
        lista2.add(m4);
        Polinom p1 = new Polinom(lista);
        assertEquals("+3,000000x^5 +0,600000x^5 +7,000000x^4 +3,000000x^4 +3,000000x^4 +0,666667x^3 +2,000000x^2 +2,000000x^2 +2,000000x^2 ",OperatiiPolinoame.integrare(p1).toString());
    }
    @Test
    public void deriv() {
        ArrayList<Monom> lista = new ArrayList<>();
        ArrayList<Monom> lista2 = new ArrayList<>();
        Monom m1 = new Monom(2, 2);
        Monom m2 = new Monom(3, 4);
        Monom m3 = new Monom(3, 5);
        Monom m4 = new Monom(4, 4);

        lista.add(m1);
        lista.add(m2);
        lista2.add(m3);
        lista2.add(m4);
        Polinom p1 = new Polinom(lista);
        assertEquals("+15,000000x^4 +28,000000x^3 +12,000000x^3 +12,000000x^3 +4,000000x^1 +4,000000x^1 +4,000000x^1 ",OperatiiPolinoame.der(p1).toString());
    }
}
package polinoame;

 

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;


public class Controller implements Initializable
{
	
	private OperatiiPolinoame op=new OperatiiPolinoame();
	String p1,p2;
	@FXML
	// The reference of txt1,txt2 will be injected by the FXML loader
	public TextField txt1, txt2;
	
	public Button adunare, scadere, inmultire, derivare, integrare;
	// The reference of rez will be injected by the FXML loader
	@FXML
	public TextField rez;
	
	// location and resources will be automatically injected by the FXML loader	
	@FXML
	public URL location;
	
	@FXML
	public ResourceBundle resources;
	
	// Add a public no-args constructor
	public Controller() 
	{

	}
	
	@FXML
	public void initialize(URL location, ResourceBundle resources){
		adunare.setOnAction(new EventHandler<ActionEvent>(){
			public void handle(ActionEvent event){
				getStrings();
				rez.setText(OperatiiPolinoame.add(crearePolinom(p1),crearePolinom(p2)).toString());
				op.clear();
			
				
			}
		});
		
		scadere.setOnAction(new EventHandler<ActionEvent>(){
			public void handle(ActionEvent event){
				getStrings();
				rez.setText(OperatiiPolinoame.sub(crearePolinom(p1),crearePolinom(p2)).toString());
				op.clear();
			
				
			}
		});
		
		inmultire.setOnAction(new EventHandler<ActionEvent>(){
			public void handle(ActionEvent event){
				getStrings();
				rez.setText(OperatiiPolinoame.inm(crearePolinom(p1),crearePolinom(p2)).toString());
				op.clear();
			
				
			}
		});
		
		derivare.setOnAction(new EventHandler<ActionEvent>(){
			public void handle(ActionEvent event){
				getStrings();
				rez.setText(OperatiiPolinoame.der(crearePolinom(p1)).toString());
				op.clear();
			
				
			}
		});
		
		integrare.setOnAction(new EventHandler<ActionEvent>(){
			public void handle(ActionEvent event){
				getStrings();
				rez.setText(OperatiiPolinoame.integrare(crearePolinom(p1)).toString());
				op.clear();
			
				
			}
		});
		
	}
	
	
	public void getStrings(){
		p1=txt1.getText();
		p2=txt2.getText();
	}
	
	public Polinom crearePolinom(String p1){
		ArrayList<Monom> lista=new ArrayList<Monom>();
		Monom m;
		
		p1=p1.replace("x^", " ");
		p1=p1.replace("+", " +");
		p1=p1.replace("-", " -");
		
		String[] a=p1.split(" ");
		float[] valori= new float[2];
		int i=0;
		
		for(String x:a){
			
			valori[i%2]=Integer.parseInt(x);
			if(i%2==1){
				m=new Monom(valori[0],(int)valori[1]);
				lista.add(m);
			}
			i++;
		}
						
		return new Polinom(lista);
		
		}
}
	
	
		
		
		

	



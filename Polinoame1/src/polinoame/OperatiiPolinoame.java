package polinoame;

 

import java.util.ArrayList;
import java.util.Collections;

public class OperatiiPolinoame {
	public OperatiiPolinoame(){
		
	}
		
private static ArrayList<Monom> lista=new ArrayList<>();

	
	public static Polinom add(Polinom p1, Polinom p2){
		for(Monom i:p1.getList()){
			lista.add(i);
		}
		for(Monom i:p2.getList()){
			lista.add(i);
		}
		Collections.sort(lista);
		for(int i =0; i<lista.size()-1;i++){
			if(lista.get(i).getExp()==lista.get(i+1).getExp()){
				lista.get(i+1).setCoef(lista.get(i).getCoef()+lista.get(i+1).getCoef());
				lista.remove(i);
				i--;
			}
		}
		return new Polinom(lista);
	}
	
	public static Polinom sub(Polinom p1, Polinom p2){
		for(Monom i:p1.getList()){
			lista.add(i);
		}
		for(Monom i:p2.getList()){
			lista.add(i);
		}
		Collections.sort(lista);
		for(int i=0; i<lista.size()-1;i++){
				if(lista.get(i).getExp()==lista.get(i+1).getExp()){
					lista.get(i+1).setCoef(lista.get(i).getCoef()-lista.get(i+1).getCoef());
					lista.remove(i);
					i--;
				}
			}

		return new Polinom(lista);
		}
	
	public static Polinom inm(Polinom p1, Polinom p2){
		ArrayList<Monom> lista1=new ArrayList<>();
		ArrayList<Monom> lista2=new ArrayList<>();
		for(Monom i:p1.getList()){
			lista.add(i);
		}
		for(Monom i:p2.getList()){
			lista1.add(i);
		}
		
		for(int i=0; i<lista.size(); i++){
			for(int j=0; j<lista1.size(); j++){
			lista2.add(new Monom(lista.get(i).getCoef()*lista1.get(j).getCoef(),lista.get(i).getExp()+lista1.get(j).getExp()));
			
			
		}
		}
		return new Polinom(lista2);
	}
	
	public static Polinom der(Polinom p1){
		for(Monom i:p1.getList()){
			lista.add(i);
		}
		Collections.sort(lista);
		for(int i=0; i<lista.size(); i++){
			if(lista.get(i).getExp()==0){
				lista.get(i).setExp(0);
				lista.get(i).setCoef(0);
			}
			lista.get(i).setCoef(lista.get(i).getCoef() * lista.get(i).getExp());
			lista.get(i).setExp(lista.get(i).getExp()-1);
			
		}
		return new Polinom(lista);
	}
	
	public static Polinom integrare(Polinom p1){
		for(Monom i:p1.getList()){
			lista.add(i);
		}
		Collections.sort(lista);
		for(int i=0; i<lista.size(); i++ ){

			lista.get(i).setCoef(lista.get(i).getCoef() / (lista.get(i).getExp()+1));
			lista.get(i).setExp(lista.get(i).getExp()+1);
		}
		return new Polinom(lista);
		
	}
	
	public void clear(){
		lista.clear();
	}
	
	
}
	


